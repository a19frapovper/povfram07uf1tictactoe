package cat.inspedralbes.madmar.tictactoe.mvpgui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;

public class DescPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblTop;
	private JLabel lblBottom;

	/**
	 * Create the panel.
	 */
	public DescPanel() {
		setLayout(new BorderLayout(0, 0));
		
		lblTop = new JLabel("Top position of description");
		lblTop.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblTop, BorderLayout.NORTH);
		
		lblBottom = new JLabel("Bottom position of desription");
		lblBottom.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblBottom, BorderLayout.SOUTH);

	}

	
	public JLabel getLblTop() {
		return lblTop;
	}
	public JLabel getLblBottom() {
		return lblBottom;
	}
}
