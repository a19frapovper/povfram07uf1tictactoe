package cat.inspedralbes.madmar.tictactoe;

import java.awt.EventQueue;

import cat.inspedralbes.madmar.tictactoe.mvpgui.MyPresenter;
import cat.inspedralbes.madmar.tictactoe.mvpgui.TicTacToeFrame;

public class LauncherGui {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {							
					LauncherGui.createGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	
	}
	
	public static void createGUI() {
		
		// Es crea la interfície gràfica (vistes)
		TicTacToeFrame frame = new TicTacToeFrame();	
		frame.setVisible(true);
		
		// MVP: Es crea el presentador.
		MyPresenter presenter=new MyPresenter();		
		
		
		frame.init(presenter);
		presenter.init(frame);
	}

}
