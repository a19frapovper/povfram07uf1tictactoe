package cat.inspedralbes.madmar.tictactoe.mvpgui;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.border.LineBorder;

public class EditPlayerColors extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel name1,name2;
	private JLabel playerColor1,playerColor2;

	/**
	 * Create the panel.
	 */
	
	public EditPlayerColors() {
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
						
		JLabel lblPlayer1 = new JLabel("Player");
		add(lblPlayer1);
		
		name1 = new JLabel("1");
		add(name1);
		
		playerColor1 = new JLabel();
		playerColor1.setBorder(new LineBorder(new Color(0, 0, 0)));
		playerColor1.setPreferredSize(new Dimension(20,20));
		playerColor1.setOpaque(true);
		System.out.println(getPlayer1color());
		playerColor1.setBackground(Color.GRAY);
		add(playerColor1);
		
		JLabel lblPlayer2 = new JLabel("Player");
		add(lblPlayer2);
		
		name2 = new JLabel("2");
		add(name2);
		
		playerColor2 = new JLabel();
		playerColor2.setBorder(new LineBorder(new Color(0, 0, 0)));
		playerColor2.setPreferredSize(new Dimension(20,20));
		playerColor2.setOpaque(true);
		playerColor2.setBackground(Color.GRAY);
		add(playerColor2);
		
		double size1= (lblPlayer1.getPreferredSize().getWidth()+name1.getPreferredSize().getWidth());
		double size2= (lblPlayer1.getPreferredSize().getWidth()+name2.getPreferredSize().getWidth());
		double max=size1+30;
		if (size2>size1)
			max=size2+30;
		setPreferredSize(new Dimension((int)max,60));
		
	}
	
	public Color getPlayer1color() {
		return playerColor1.getBackground();
	}
	public void setPlayer1color(Color color) {
		playerColor1.setBackground(color);
	}
	
	public Color getPlayer2color() {
		return playerColor2.getBackground();
	}
	public void setPlayer2color(Color color) {
		playerColor2.setBackground(color);
	}
}
