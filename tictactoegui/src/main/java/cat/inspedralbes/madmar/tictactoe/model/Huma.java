package cat.inspedralbes.madmar.tictactoe.model;

import java.awt.Color;

public class Huma extends Jugador {
	
	public Huma(String name,char symbol, Color color) {
		super(name,symbol,color);
	}
	
	@Override
	protected Jugada obtenirJugadaGui(String posicio) {
		
		String[] parts=posicio.split(",");
		
		int fila = Integer.parseInt(parts[0]);
		int col= Integer.parseInt(parts[1]);
		
		return new Jugada(fila, col);
	}
	
	//Aquest toString sempre retorna el nom de la classe
	@Override
	public String toString() {
			return getClass().getSimpleName();
	}

}
