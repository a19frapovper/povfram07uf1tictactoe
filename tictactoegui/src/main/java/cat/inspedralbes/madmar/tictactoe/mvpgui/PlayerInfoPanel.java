package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.border.LineBorder;

public class PlayerInfoPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelName;
	private JLabel lblSymbol;
	private JLabel lblYourTurn;
	private JLabel lblColor;
	
	private EditPlayerColors editColors;

	/**
	 * Create the panel.
	 */
	public PlayerInfoPanel() {
		
		editColors = new EditPlayerColors();
		
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(10);
		flowLayout.setHgap(10);
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		setPreferredSize(new Dimension(150,200));
		
		JLabel lblPlayerText = new JLabel("Player:");
		lblPlayerText.setPreferredSize(new Dimension(60,30));
		add(lblPlayerText);
		
		labelName = new JLabel("1");
		labelName.setPreferredSize(new Dimension(70,30));
		add(labelName);
		
		JLabel lblSymbolText = new JLabel("Symbol:");
		lblSymbolText.setPreferredSize(new Dimension(60,30));
		add(lblSymbolText);
		
		lblSymbol = new JLabel("X");
		add(lblSymbol);
		
		JLabel lblColorText = new JLabel("Color:");
		lblColorText.setPreferredSize(new Dimension(60,30));
		add(lblColorText);
		
		lblColor = new JLabel("");
		lblColor.setOpaque(true);
		lblColor.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblColor.setPreferredSize(new Dimension(20, 20));
		lblColor.setBackground(editColors.getPlayer1color());
		add(lblColor);
		
		lblYourTurn = new JLabel("Your turn");
		add(lblYourTurn);

	}

	public String getPlayerName() {
		return labelName.getText();
	}
	public void setPlayerName(String text) {
		labelName.setText(text);
	}
	public String getPlayerSymbol() {
		return lblSymbol.getText();
	}
	public void setPlayerSymbol(String text) {
		lblSymbol.setText(text);
	}
	public Color getPlayerColor() {
		return lblColor.getBackground();
	}
	public void setPlayerColor(Color color) {
		lblColor.setBackground(color);
	}
	public boolean isLblYourTurnVisible() {
		return lblYourTurn.isVisible();
	}
	public void setLblYourTurnVisible(boolean visible) {
		lblYourTurn.setVisible(visible);
	}


}
