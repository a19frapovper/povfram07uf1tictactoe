package cat.inspedralbes.madmar.tictactoe.model;

//Aquesta classe s'encarrega de gestionar la part fisica del joc de tres en ratlla
public class Tauler {

	private static final int FITXA_A = 1;
	private static final int FITXA_B = 2;
	public static final int BUIT = 0; // Serveix per a empat

	//L'estat del tauler es guarda en un array 2D de enters. 
	//Si hi ha un 1 es el jugador A. Si hi ha un 2 es el jugador B. Si hi ha un 0 esta buit. Utilitzem constants.
	//Aquestos codis tambe s'utilitzen per a indicar si hi ha guanyador
	private int[][] caselles;

	//Inicialment no hi ha guanyador
	private int guanyador = BUIT;

	//Constructor
	public Tauler() {
		caselles = new int[3][3];
	}

	//Si la casella esta buida, es posa.
	//El return indica si s'ha fet o no.
	public boolean posarFitxa(int fila, int col, int fitxa) {
		if (caselles[fila][col] == BUIT) {
			caselles[fila][col] = fitxa;
			return true;
		}
		return false;
	}
	
	public boolean esFinalPartida() {
		// Els seguents metodes s'executen tots sempre
		// Es podria fer que retornessin un boolea i parar la comprovacio 
		// al primer que detectes final de partida.
		
		boolean result=false;
		
		comprovarFiles(FITXA_A);
		comprovarFiles(FITXA_B);
		comprovarColumnes(FITXA_A);
		comprovarColumnes(FITXA_B);
		comprovarDiagonals(FITXA_A);
		comprovarDiagonals(FITXA_B);

		if (guanyador != BUIT)
			result=true;

		if (taulerPle()) {
			result=true;
		}

		return result;
	}

	//Mirem totes les caselles. Si es troba alguna buida, es false. 
	//Si arriba al final i no ha hagut buida, taulerPle es true.
	private boolean taulerPle() {

		for (int fila = 0; fila < caselles.length; fila++) {
			for (int col = 0; col < caselles[0].length; col++) {
				if(caselles[fila][col] == BUIT) {
					return false;
				}
			}
		}
		return true;

	}

	// Les diagonals es miren hardcoded
	// Les igualtats es fan amb == ja que son enters
	// Si fossin String es farien amb equals()
	private void comprovarDiagonals(int fitxa) {
		if ((caselles[0][0] == caselles[1][1] && caselles[1][1] == caselles[2][2] && caselles[2][2] == fitxa)||
			 caselles[0][2] == caselles[1][1] && caselles[1][1] == caselles[2][0] && caselles[2][0] == fitxa) {
			guanyador = fitxa;
		} 

	}

	//Les files i columnnes es miren en bucle.
	private void comprovarFiles(int fitxa) {
		for (int i = 0; i < caselles.length; i++) {
			if (caselles[i][0] == caselles[i][1] && caselles[i][1] == caselles[i][2] && caselles[i][2] == fitxa) {
				guanyador = fitxa;
				break;
			}
		}

	}

	private void comprovarColumnes(int fitxa) {
		for (int i = 0; i < caselles.length; i++) {
			if (caselles[0][i] == caselles[1][i] && caselles[1][i] == caselles[2][i] && caselles[2][i] == fitxa) {
				guanyador = fitxa;
				break;
			}
		}

	}

	public int obtenirGuanyador() {
		return guanyador;
	}

	//El toString retorna una quadricula amb el tauler
	@Override
	public String toString() {
		String string = "";
		for (int fila = 0; fila < caselles.length; fila++) {
			string += "|";
			for (int col = 0; col < caselles[0].length; col++) {
				string += " " + obteFitxaEnString(caselles[fila][col]) + " |";
			}
			string += "\n";
		}
		return string;
	}

	// Com l'estat del tauler son enters, a l'hora de mostrar queda millor si es veuen Strings X O i espais.
	// Aquest metode assigna una String a un enter.
	// Es static perque no depen de l'estat de l'objecte. El calcul es exactament el mateix per a tots 
	// els objectes de la classe. Per tant, es pot posar estatic.
	public static String obteFitxaEnString(int i) {
		switch (i) {
		case FITXA_A:
			return "X";
		case FITXA_B:
			return "O";
		default:
			return " ";
		}

	}

	//Aquest metode es utilitzat per les IA per a calcular les jugades
	//Es podria fer un metode int getCasella(int, int) tambe
	public int[][] getMatriu() {
		return caselles;
	}
}
