package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JToolBar;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TicTacToeFrame extends JFrame implements ActionListener,IFrameView{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BoardPanel board;
	private PlayerInfoPanel[] playersInfo = new PlayerInfoPanel[2];
	private JLabel lblWinner;	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JMenu mnMode,mnView;
	
	private EditPlayerSymbols editSymbols;
	private EditPlayerNames editNames;
	
	private JRadioButtonMenuItem rdbtnmntmHumanVsAi;
	private MyPresenter presenter;
	private JPanel contentPane;
	private JMenuItem mntmLoadSavedGame;
	private EditPlayerColors editColors;
	
	/**
	 * Create the frame.
	 */
	public TicTacToeFrame() {
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				
				Object[] option = { "Yes", "No", "Cancel" };
				
				int n = JOptionPane.showOptionDialog(
						null, 
						"Close the game?", 
						"", 
						JOptionPane.YES_NO_CANCEL_OPTION, 
						JOptionPane.QUESTION_MESSAGE, 
						null, 
						option, 
						option[2]);
				
				if( n == 0 ) {
					dispose();
				}
			}
		});
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnGame = new JMenu("Game");
		menuBar.add(mnGame);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(this);
		mnGame.add(mntmNew);
		
		mntmLoadSavedGame = new JMenuItem("Load saved game");
		mntmLoadSavedGame.setActionCommand("load");
		mntmLoadSavedGame.addActionListener(this);
		mnGame.add(mntmLoadSavedGame);
		
		JMenuItem mntmClose = new JMenuItem("Close");
		mntmClose.addActionListener(this);
		mnGame.add(mntmClose);
		
		mnMode = new JMenu("Mode");
		menuBar.add(mnMode);
		
		rdbtnmntmHumanVsAi = new JRadioButtonMenuItem("Human vs AI");
		buttonGroup.add(rdbtnmntmHumanVsAi);
		mnMode.add(rdbtnmntmHumanVsAi);
		rdbtnmntmHumanVsAi.setSelected(true);
		
		JRadioButtonMenuItem rdbtnmntmHumanVsHuman = new JRadioButtonMenuItem("Human vs Human");
		buttonGroup.add(rdbtnmntmHumanVsHuman);
		mnMode.add(rdbtnmntmHumanVsHuman);
		
		JMenu mnIaLevel = new JMenu("IA Level");
		menuBar.add(mnIaLevel);
		
		JMenuItem mntmEasy = new JMenuItem("Easy");
		mnIaLevel.add(mntmEasy);
		
		JMenuItem mntmMiddle = new JMenuItem("Middle");
		mnIaLevel.add(mntmMiddle);
		
		JMenuItem mntmHard = new JMenuItem("Hard");
		mnIaLevel.add(mntmHard);
		
		mnView = new JMenu("View");
		menuBar.add(mnView);
		
		JMenuItem mntmSymbols = new JMenuItem("Symbols");
		mntmSymbols.addActionListener(this);
		mnView.add(mntmSymbols);
		
		JMenuItem mntmPlayerNames = new JMenuItem("Player names");
		mntmPlayerNames.setActionCommand("names");
		mntmPlayerNames.addActionListener(this);
		mnView.add(mntmPlayerNames);
		
		JMenuItem mntmPlayerTilesColor = new JMenuItem("Player tiles color");
		mntmPlayerTilesColor.setActionCommand("colors");
		mntmPlayerTilesColor.addActionListener(this);
		mnView.add(mntmPlayerTilesColor);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(this);
		mnHelp.add(mntmAbout);
		
		JMenuItem mntmOnlineHelp = new JMenuItem("Online help");
		mntmOnlineHelp.setActionCommand("link");
		mntmOnlineHelp.addActionListener(this);
		mnHelp.add(mntmOnlineHelp);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel top = new JPanel();
		contentPane.add(top, BorderLayout.NORTH);
		
		JToolBar toolBar = new JToolBar();
		top.add(toolBar);
		
		JPanel bottom = new JPanel();
		contentPane.add(bottom, BorderLayout.SOUTH);
		
		lblWinner = new JLabel("Guanyador: ");
		bottom.add(lblWinner);
		
		PlayerInfoPanel left = new PlayerInfoPanel();
		contentPane.add(left, BorderLayout.WEST);
				
		PlayerInfoPanel right = new PlayerInfoPanel();
		right.setPlayerName("2");
		right.setPlayerSymbol("O");
		right.setPlayerColor(Color.GRAY);
		contentPane.add(right, BorderLayout.EAST);
		
		JPanel center = new JPanel();
		contentPane.add(center, BorderLayout.CENTER);
		
		board = new BoardPanel();
		board.setPreferredSize(new Dimension(250, 250));
		center.add(board);
		
		editSymbols = new EditPlayerSymbols();
		playersInfo[0]=left;
		playersInfo[1]=right;
		
		editNames = new EditPlayerNames();
		playersInfo[0]=left;
		playersInfo[1]=right;
		
		editColors = new EditPlayerColors();
		
		clearGUI();
		
		pack();		
			
	}

	public void init(MyPresenter presenter)
	{
		this.presenter=presenter;
		board.init(presenter);
	}
	
	// Mètodes de modificació de la GUI iniciats a través d'esdeveniments
	// Obtenir informació de la GUI		
	// Cridar al presentador	
	@Override
	public void actionPerformed(ActionEvent arg0) {

		System.out.println(arg0.getActionCommand());
		switch (arg0.getActionCommand()) {
			default:			
			case "New":						
				onclickNewGame();
				break;
			case "Symbols":		
				onClickEditSymbols();				
				break;
			case "names":
				onClickEditNames();
				break;
			case "colors":
				onClickEditColors();
				break;
			case "Close":
				this.dispose();
				break;
			case "About":
				onClickAbout();
				break;
			case "load":
				onClickLoadSavedGame();
				break;
			case "link":
				JOptionPane.showMessageDialog(this, "<html>Can find help at <a href=\"www.google.cat\">www.javipoveda.com</a></html>");
				break;
			case "back":
				setContentPane(contentPane);
				pack();
				break;
		}		
	}

	private void onClickLoadSavedGame() {
		JFileChooser fileChooser = new JFileChooser();
	    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	    fileChooser.showSaveDialog(mntmLoadSavedGame);
	}

	private void onClickAbout() {
		
		int cpWidth  = contentPane.getWidth();
		int cpHeight = contentPane.getHeight();
		
		AboutDialogPanel aboutPanel = new AboutDialogPanel();
		aboutPanel.setPreferredSize(new Dimension(cpWidth, cpHeight));
		
		aboutPanel.getBtnLicense().setText("License");
		aboutPanel.getBtnCredits().setText("Credits");
		
		aboutPanel.getBtnBack().setText("Return to game");
		aboutPanel.getBtnBack().setActionCommand("back");
		aboutPanel.getBtnBack().addActionListener(this);
		
		setContentPane(aboutPanel);
		pack();
		
	}

	private void onclickNewGame() {
		presenter.onClickNewGame();
	}
		
	private void onClickEditSymbols()
	{
		Object[] options = {"Edit","Cancel"};		
		
		editSymbols.setPlayer1symbol(playersInfo[0].getPlayerSymbol());
		editSymbols.setPlayer2symbol(playersInfo[1].getPlayerSymbol());
		
		if (JOptionPane.showOptionDialog(null,editSymbols,"Player symbols", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,options,options[0])==0)
		{
			presenter.onClickEditSymbols(editSymbols.getPlayer1symbol(),editSymbols.getPlayer2symbol());				
		}				
	}
	

	private void onClickEditNames() 
	{
		Object[] options = {"Edit", "Cancel"};
		
		editNames.setPlayer1name(editNames.getPlayer1name());
		editNames.setPlayer2name(editNames.getPlayer2name());
		
		if (JOptionPane.showOptionDialog(null,editNames,"Player names", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,options,options[0])==0)
		{
			presenter.onClickEditNames(editNames.getPlayer1name(), editNames.getPlayer2name());				
		}	
	}

	private void onClickEditColors() {
		Object[] options = {"Edit", "Cancel"};

		Color color  = JColorChooser.showDialog(this, "Player 1 color", Color.BLACK);
		editColors.setPlayer1color(color);
		Color color2 = JColorChooser.showDialog(this, "Player 2 color", Color.BLACK);
		editColors.setPlayer2color(color2);
		
		System.out.println(editColors.getPlayer1color());
		
		if (JOptionPane.showOptionDialog(null,editColors,"Player names", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,options,options[0])==0)
		{
			presenter.onClickEditColors(editColors.getPlayer1color(), editColors.getPlayer2color());				
		}

	}
	
	// Mètodes de modificació de la GUI definits a la interface i iniciats al Presentador	
	@Override
	public void clearGUI() {			
		enableOptionChanges(true);
		showTurn(true); 			
		hideWinner();
		board.clearBoard();
	}
		
	@Override
	public void hideTurn() {
		setTurnPlayer1(false);
		setTurnPlayer2(false);		
	}

	@Override
	public void showTurn(boolean visibility) {
		setTurnPlayer1(visibility);
		setTurnPlayer2(!visibility);
	}

	@Override
	public void showWinner(String text) {		
		setWinnerInfo(text);
		setWinnerInfoVisible(true);	
		
		Object[] option = { "Yes", "No"};
		
		int n = JOptionPane.showOptionDialog(
				this, 
				"Play again?", 
				"", 
				JOptionPane.YES_NO_OPTION, 
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				option, 
				option[0]);
		
		if( n == 1 ) {
			dispose();
		} else {
			onclickNewGame();
		}
	}
	
	@Override
	public void hideWinner() {
		lblWinner.setVisible(false);		
	}
	
	@Override
	public void enableOptionChanges(boolean enabled) {
		enableMode(enabled);
		enableView(enabled);
	}
	
	@Override
	public void setPlayersSymbols(char s1,char s2) {
		playersInfo[0].setPlayerSymbol(s1+"");
		playersInfo[1].setPlayerSymbol(s2+"");
	}

	@Override
	public String[] getPlayersSymbol() {		
		String[] symbols=new String[2];
		symbols[0]=playersInfo[0].getPlayerSymbol();
		symbols[1]=playersInfo[1].getPlayerSymbol();		
		return symbols;
	}

	@Override
	public void setPlayersName(String s1, String s2) {
		playersInfo[0].setPlayerName(s1);
		playersInfo[1].setPlayerName(s2);
	}
	
	@Override
	public String[] getPlayersName() {
		
		String[] names = new String[2];
		
		names[0] = playersInfo[0].getPlayerName();
		names[1] = playersInfo[1].getPlayerName();		
		
		return names;
	}
	
	@Override
	public void setPlayersColor(Color player1color, Color player2color) {
		playersInfo[0].setPlayerColor(player1color);
		playersInfo[1].setPlayerColor(player2color);
	}
	
	@Override
	public Color[] getPlayersColor() {
		
		Color[] colors = new Color[2];
		
		colors[0] = playersInfo[0].getPlayerColor();
		colors[1] = playersInfo[1].getPlayerColor();
		
		return colors;
	}
	
	@Override
	public boolean isIAModeSelected() {				
		return rdbtnmntmHumanVsAi.isSelected();
	}
	
	public boolean isWinnerInfoVisible() {
		return lblWinner.isVisible();
	}

	public BoardPanel getBoard() {
		return board;
	}
	
	public String getWinnerInfo() {
		return lblWinner.getText();
	}
	
	// Altres m�todes
	private void setWinnerInfo(String text) {
		lblWinner.setText(text);
	}

	private void setWinnerInfoVisible(boolean visible) {
		lblWinner.setVisible(visible);
	}
	
	private void setTurnPlayer1(boolean visibility) {
		playersInfo[0].setLblYourTurnVisible(visibility);
	}
	
	private void setTurnPlayer2(boolean visibility) {
		playersInfo[1].setLblYourTurnVisible(visibility);
	}	

	private void enableMode(boolean enabled) {
		mnMode.setEnabled(enabled);
	}

	private void enableView(boolean enabled) {		
		mnView.setEnabled(enabled);
	}

}
