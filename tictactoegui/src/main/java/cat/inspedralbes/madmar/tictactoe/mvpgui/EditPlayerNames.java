package cat.inspedralbes.madmar.tictactoe.mvpgui;

import javax.swing.JPanel;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.FlowLayout;

public class EditPlayerNames extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel name1,name2;
	private JTextField realName1,realName2;

	/**
	 * Create the panel.
	 */
	
	public EditPlayerNames() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
						
		JLabel lblPlayer1 = new JLabel("Player");
		add(lblPlayer1);
		
		name1 = new JLabel("1");
		add(name1);
		
		realName1 = new JTextField();
		realName1.setPreferredSize(new Dimension(50,20));
		add(realName1);
		
		JLabel lblPlayer2 = new JLabel("Player");
		add(lblPlayer2);
		
		name2 = new JLabel("2");
		add(name2);
		
		realName2 = new JTextField();
		realName2.setPreferredSize(new Dimension(50,20));
		add(realName2);
		
		double size1= (lblPlayer1.getPreferredSize().getWidth()+name1.getPreferredSize().getWidth()+realName1.getPreferredSize().getWidth());
		double size2= (lblPlayer1.getPreferredSize().getWidth()+name2.getPreferredSize().getWidth()+realName2.getPreferredSize().getWidth());
		double max=size1+30;
		if (size2>size1)
			max=size2+30;
		setPreferredSize(new Dimension((int)max,60));
		
	}
	
	public String getPlayer1name() {
		return realName1.getText();
	}
	public void setPlayer1name(String text) {
		realName1.setText(text);
	}
	
	public String getPlayer2name() {
		return realName2.getText();
	}
	public void setPlayer2name(String text) {
		realName2.setText(text);
	}
}
