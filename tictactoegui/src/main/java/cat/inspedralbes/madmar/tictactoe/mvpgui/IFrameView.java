package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Color;

public interface IFrameView {
	
	public void showWinner(String text);
	public void hideWinner();
	public void showTurn(boolean visibility);
	public void hideTurn();
	public void clearGUI();
	public void enableOptionChanges(boolean enabled);
	public void setPlayersSymbols(char s1,char s2);
	public void setPlayersName(String s1, String s2);
	public void setPlayersColor(Color player1color, Color player2color);
	public boolean isIAModeSelected();
	public String[] getPlayersSymbol();
	public String[] getPlayersName();
	public Color[] getPlayersColor();
	public BoardPanel getBoard();

}
