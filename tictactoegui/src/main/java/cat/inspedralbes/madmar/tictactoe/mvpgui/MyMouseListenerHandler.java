package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MyMouseListenerHandler implements MouseListener{
	
	private Color colorOriginalCela;
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		Component component = arg0.getComponent();
		colorOriginalCela = component.getBackground();
		
		if(component.isEnabled()) {
			component.setBackground(Color.GRAY);			
		}
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		Component component = arg0.getComponent();
		component.setBackground(colorOriginalCela);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
}
